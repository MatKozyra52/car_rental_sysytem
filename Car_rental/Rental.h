#ifndef RENTAL_H
#define RENTAL_H


#include "Utils.h"


////////File location and its names
#define source "./Data/cars.txt"
#define usersBase "./Data/users.txt"
#define globalID "./Data/nextID.txt"
#define orders "./Data/orders.txt"
////////////////////////////////////

////////Car class
class Car {
private:
    string m_mark;
    string m_model;
    string m_ID; 
    int m_year;
    int m_price;
    int m_mileage;                  /*in km*/
    string m_body_type;
    string m_details;               /*additional information*/
    int m_owner;

public:
    //Constructor
    Car(const string& mark = " ", const string& model = " ", const string& ID = " ", int year = 0,
        int price = 0, int mileage = 0, const string& body_type = " ", const string& details = " ", int owner = 0) :
        m_mark{ mark }, m_model{ model }, m_ID{ ID }, m_year{ year }, m_price{ price }, m_mileage{mileage}, m_body_type{ body_type },
        m_details{ details }, m_owner{owner} {}

    //Seters
    void set_mark(const string& mark) { m_mark = mark; }
    void set_model(const string& model) { m_model = model; }
    void set_ID(const string& ID) { m_ID = ID; }
    void set_year(const int year) { m_year = year; }
    void set_price(const int price) { m_price = price; }
    void set_mileage(const int mileage) { m_mileage = mileage; }
    void set_body_type(const string& body_type) { m_body_type = body_type; }
    void set_details(const string& details) { m_details = details; }
    void set_owner(const int owner) { m_owner = owner; }

    //Geters
    string get_mark(void) const { return m_mark; }
    string get_model(void) const { return m_model; }
    string get_ID(void) const { return m_ID; }
    int get_year(void) const { return m_year; }
    int get_price(void) const { return m_price; }
    int get_mileage(void) const { return m_mileage; }
    string get_body_type(void) const { return m_body_type; }
    string get_details(void) const { return m_details; }
    int get_owner(void) const { return m_owner; }

    /**
     * Print in one line main information : MARK, MODEL, YEAR, PRICE
     */
    void print_list();

    /**
     * Print every information expect ID information
     */
    void print_details();

    /**
     * Print every information holded in class
     */
    void print_admin();

    /**
     * Overloaded operator
     */
    Car& operator= (const Car& rhs);
};


////////User class
class User {
private:
    int m_ID;
    string m_username;
    string m_pass;
    string m_email;
    string m_name;
    string m_surname;
    string m_address;
    string m_IDnumber;              /*ID Document number*/
    string m_phone;
    string m_birthdate;

public:
    //Constructor
    User(int ID = 0, const string& username = " ", const string& pass = " ", const string& email = " ", const string& name = " ", const string& surname = " ",
        const string& address = " ", const string& IDnumber = " ", const string& phone = " ", const string& birthdate = " ") :
        m_ID{ ID }, m_username{ username }, m_pass{ pass }, m_email{ email }, m_name{ name }, m_surname{surname},
        m_address{ address }, m_IDnumber{ IDnumber }, m_phone{ phone }, m_birthdate{ birthdate } {}

    //Seters
    void set_ID(int ID) { m_ID = ID; }
    void set_username(const string& username) { m_username = username; }
    void set_pass(const string& pass) { m_pass = pass; }
    void set_email(const string& email) { m_email = email; }
    void set_name(const string& name) { m_name = name; }
    void set_surname(const string& surname) { m_surname = surname; }
    void set_address(const string& address) { m_address = address; }
    void set_IDnumber(const string& IDnumber) { m_IDnumber = IDnumber; }
    void set_phone(const string& phone) { m_phone = phone; }
    void set_birthdate(const string& birthdate) { m_birthdate = birthdate; }

    //Geters
    int get_ID(void) const { return m_ID; }
    string get_username(void) const { return m_username; }
    string get_pass(void) const { return m_pass; }
    string get_email(void) const { return m_email; }
    string get_name(void) const { return m_name; }
    string get_surname(void) const { return m_surname; }
    string get_address(void) const { return m_address; }
    string get_IDnumber(void) const { return m_IDnumber; }
    string get_phone(void) const { return m_phone; }
    string get_birthdate(void) const { return m_birthdate; }
    

    /**
    * Overloaded operator
    */
    User& operator= (const User& rhs);

    /**
     * Print information about this user 
     */
    void print_details(void);


};

////////Order class
class Order {
private:
    string m_car_ID;
    int m_user_ID;
    string m_date;
    int m_days;

public:
    //Constructor
    Order(const string& car_ID, int user_ID, const string& date, int days ) :
        m_car_ID{ car_ID }, m_user_ID{ user_ID }, m_date{ date }, m_days{ days }{}

    //Seters
    void set_car_ID(const string& car_ID) { m_car_ID = car_ID;}
    void set_user_ID(int user_ID) { m_user_ID = user_ID; }
    void set_date(const string& date) { m_date = date; }
    void set_days(int days) { m_days = days; }

    //Geters
    string get_car_ID(void) const { return m_car_ID; }
    int get_user_ID(void) const { return m_user_ID; }
    string get_date(void) const { return m_date; }
    int get_days(void) const { return m_days; }

    /**
    * Overloaded operator
    */
    Order& operator= (const Order& rhs);

    /**
     * Print in one line information about order
     */
    void print_list();

    /**
     * Print detailed information
     *
     * @param information from our bases in vectors form
     */
    void print_details(vector<User>* users, vector<Car>* cars);
};


/**
 * Add car to the base. User writes every parameter, and then data are writen to file and returned in object form
 *
 * @return object made from user data
 */
Car addCar();

/**
 * Add user to the base. User writes every parameter, and then data are saved in file and returned in object form
 *
 * @return object made from user data
 */
User addUser();

/**
 * Create Car class' object from file. Function takes line of data saved in file and creates single object
 *
 * @param "pos" indicates starting position in file - it is also updated after reading
 *        "eof" indicates if end of file was achieved
 * @return object created from data
 */
Car loadCar(streampos* pos, bool* eof);

/**
 * Create Uses class' object from file. Function takes line of data saved in file and creates single object
 *
 * @param "pos" indicates starting position in file - it is also updated after reading
 *        "eof" indicates if end of file was achieved
 * @return object created from data
 */
User loadUser(streampos* pos, bool* eof);

/**
 * Create Order class' object from file. Function takes line of data saved in file and creates single object
 *
 * @param "pos" indicates starting position in file - it is also updated after reading
 *        "eof" indicates if end of file was achieved
 * @return object created from data
 */
Order loadOrder(streampos* pos, bool* eof);

/**
 * Write vector of Car objects into file
 *
 * @param pointer to vector
 */
void saveCarVector(vector<Car> *base);

/**
 * Write vector of User objects into file
 *
 * @param pointer to vector
 */
void saveUserVector(vector<User> *base);

/**
 * Write vector of Order objects into file
 *
 * @param pointer to vector
 */
void saveOrderVector(vector<Order>* base);

/**
 * Print information about particular user
 *
 * @param user ID number
 */
void findUser(vector<User>* base, int id);

/**
 * Print information about particular car
 *
 * @param car ID number
 */
void findCarID(vector<Car>* base, string id);

/**
 * Full login procedure - check if user is in base
 *
 * @return user ID
 */
int login();

/**
 * Add new order to the base
 *
 * @param pointer to particular car object
 */
void rentCar(Car* vehicle);

/**
 * Filter data in vector and delete rented cars
 *
 * @param pointer to vector with full list of cars
 */
void deleteNotAva(vector<Car>* base);

/**
 * Sort vector procedure
 *
 * @param Vector to sort
 * @return Sorted vector
 */
vector<Car> sortit(vector<Car> base);

#endif