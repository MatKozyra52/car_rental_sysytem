#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <ctime>
#include <algorithm>

using std::cout;
using std::string;
using std::endl;
using std::cin;
using std::getline;
using std::streampos;
using std::vector;
using std::getline;
using std::ws;
using std::ios;
using std::fstream;
using std::ios_base;

/**
 * Get int value from user
 *
 * @return int value from user
 */
int cin_value();

/**
 * Print main menu
 *
 * @param actually logged user's ID
 * @return option selected by user
 */
int hello(int user);

#endif