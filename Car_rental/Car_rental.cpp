﻿#include "Rental.h"



int main()
{

    streampos pos{ ios_base::beg };                                     /*Position in file - beginning*/
    bool eof{0};                                                        /*Indicates end of file*/
    vector<Car> base;                                                   /*Car vector*/
    vector<User> users;                                                 /*User vector*/
    vector<Order> oferts;                                               /*Orders vector*/
    int option{ 0 };                                                    /*Position in menu*/
    int user {0};                                                       /*Logged user ID*/

    pos = ios_base::beg;
    while (!eof)  base.push_back(loadCar(&pos, &eof));                  /*Load car base*/
    sort(base.begin(), base.end(), [](Car x, Car y) { return x.get_mark() < y.get_mark(); });

    pos = ios_base::beg; 
    eof = 0;
    while (!eof)  users.push_back(loadUser(&pos, &eof));                /*Load user base*/
    if (users[0].get_ID() == 0) {                                       /*Check if correct*/
        cout << "There is no admin user in base";
        exit(1);
    }


    //OPTIONS
    while (option != 99) {
        option = hello(user);                                           /*Home screen*/

        //Car list
        if (option == 1) {
            bool back{ 0 };
            int idx{ 0 };                                               /*Car's number*/
            while (!back) {
                system("cls");
                idx = 0;
                deleteNotAva(&base);
                for (auto i : base) { cout << ++idx << "\t";  i.print_list(); }             /*Print out cars*/

                cout << "\n\n0\tBACK\n-1\tSORT";
                cout << "\n\nI would know more about <car's number>: ";
                option = cin_value();
                if (!option) back = 1;
                else if (option == -1) base = sortit(base);
                else if (option > 0 && unsigned(option) <= base.size()) {
                    system("cls");
                    base[--option].print_details();
                    cout << "\n\n\nType 1 to rent this car" << endl;
                    char buf;
                    cin >> buf;             //just to wait for back
                    if (buf == '1') {
                        string carID = base[option].get_ID();
                        pos = ios_base::beg;
                        eof = 0;
                        base.clear();
                        while (!eof)  base.push_back(loadCar(&pos, &eof));
                        for (int i = 0; unsigned(i) < base.size(); i++) if (base[i].get_ID() == carID) rentCar(&base[i]);
                        saveCarVector(&base);
                    }
                }
            }
            pos = ios_base::beg;                                        /*Load again because base was modified*/
            eof = 0;
            base.clear();
            while (!eof)  base.push_back(loadCar(&pos, &eof));
        }

        //Register
        else if (option == 2) {
            addUser();                                                  /*Register*/
            pos = ios_base::beg;                                        /*Reload base*/
            eof = 0;
            users.clear();
            while (!eof)  users.push_back(loadUser(&pos, &eof));
        }

        //Login
        else if (option == 3) {
            user = login();                                             /*Return logged user ID*/
        }

        //Account details
        else if (option == 4) {
            bool back{ 0 };
            while (!back) {

                system("cls");
                users[user - 1].print_details();
                cout << "\n\n0. BACK\n1. USERNAME\n2. PASSWORD\n3. E-MAIL\n4. NAME\n5. SURNAME\n6. ADDRESS\n7. ID_NUMBER\n8. PHONE\n9. BIRTHDATE" << endl;
                cout << "\n\n Parametr:\t";
                int input{ 0 };
                bool username_in_use = 1;
                input = cin_value();

                if (!input) back = 1;
                else if (input > 0 && input < 10) {
                    string info;
                    int value{ 0 };
                    switch (input) {
                    case 1:
                        while (username_in_use) {
                            cout << "USERNAME:";
                            getline(cin >> ws, info);
                            username_in_use = 0;
                            for (auto i : users)
                                if (info == i.get_username()) {
                                    cout << "Username already in use\n Try another name\n\n";
                                    username_in_use = 1;
                                }
                        }
                        users[user - 1].set_username(info);
                        break;
                    case 2:
                        cout << "PASSWORD: ";
                        getline(cin >> ws, info);
                        users[user - 1].set_pass(info);
                        break;
                    case 3:
                        cout << "E-MAIL: ";
                        getline(cin >> ws, info);
                        users[user - 1].set_email(info);
                        break;
                    case 4:
                        cout << "NAME: ";
                        getline(cin >> ws, info);
                        users[user - 1].set_name(info);
                        break;
                    case 5:
                        cout << "SURNAME: ";
                        getline(cin >> ws, info);
                        users[user - 1].set_surname(info);
                        break;
                    case 6:
                        cout << "ADDRESS: ";
                        getline(cin >> ws, info);
                        users[user - 1].set_address(info);
                        break;
                    case 7:
                        cout << "ID_NUMBER: ";
                        getline(cin >> ws, info);
                        users[user - 1].set_IDnumber(info);
                        break;
                    case 8:
                        cout << "PHONE: ";
                        getline(cin >> ws, info);
                        users[user - 1].set_phone(info);
                        break;
                    case 9:
                        cout << "BIRTHDATE: ";
                        getline(cin >> ws, info);
                        users[user - 1].set_birthdate(info);
                        break;
                    }


                    saveUserVector(&users);
                }

            }
        }

        //Add car
        else if (option == 11) {
            addCar();
            //Reload base
            base.clear();
            pos = ios_base::beg;
            eof = 0;
            while (!eof)  base.push_back(loadCar(&pos, &eof));
            sort(base.begin(), base.end(), [](Car x, Car y) { return x.get_mark() < y.get_mark(); }); //sort
        }

        //Edit car
        else if (option == 12) {

            bool back{ 0 };
            int idx{ 0 };                                               /*Car's number*/
            while (!back) {
                idx = 0;
                system("cls");
                for (auto i : base) { cout << ++idx << "\t";  i.print_list(); }

                cout << "\n\n0\tBACK\n-1\tSORT";
                cout << "\n\nEdit <car's number>: ";
                option = cin_value();
                if (!option) back = 1;
                else if (option == -1) base = sortit(base);
                else if (option > 0 && unsigned(option) <= base.size()) {
                    system("cls");
                    base[option - 1].print_admin();
                    cout << "\n\n0\tBACK\n1\tMARK\n2\tMODEL\n3\tID\n4\tYEAR\n5\tPRICE\n6\tMILEAGE\n7\tBODY_TYPE\n8\tDETAILS\n9\tOWNER" << endl;
                    cout << "\n\n Parametr:\t";
                    int input{ 0 };
                    input = cin_value();

                    if (input > 0 && input < 10) {
                        string info;
                        int value{ 0 };
                        switch (input) {
                        case 1:
                            cout << "MARK: ";
                            getline(cin >> ws, info);
                            base[option - 1].set_mark(info);
                            break;
                        case 2:
                            cout << "MODEL: ";
                            getline(cin >> ws, info);
                            base[option - 1].set_model(info);
                            break;
                        case 3:
                            cout << "ID: ";
                            getline(cin >> ws, info);
                            base[option - 1].set_ID(info);
                            break;
                        case 4:
                            cout << "YEAR: ";
                            value = cin_value();
                            base[option - 1].set_year(value);
                            break;
                        case 5:
                            cout << "PRICE: ";
                            value = cin_value();
                            base[option - 1].set_price(value);
                            break;
                        case 6:
                            cout << "MILEAGE: ";
                            value = cin_value();
                            base[option - 1].set_mileage(value);
                            break;
                        case 7:
                            cout << "BODY_TYPE: ";
                            getline(cin >> ws, info);
                            base[option - 1].set_body_type(info);
                            break;
                        case 8:
                            cout << "DETAILS: ";
                            getline(cin >> ws, info);
                            base[option - 1].set_details(info);
                            break;
                        case 9:
                            cout << "OWNER: ";
                            value = cin_value();
                            base[option - 1].set_owner(value);
                            break;
                        }


                        saveCarVector(&base);
                    }
                }
            }
        }

        //Delete car
        else if (option == 13) {

            bool back{ 0 };
            int idx{ 0 };
            while (!back) {
                idx = 0;
                system("cls");
                for (auto i : base) { cout << ++idx << "\t";  i.print_list(); }

                cout << "\n\n0\tBACK";
                cout << "\n\nDelete <car's number>: ";
                option = cin_value();
                if (!option) back = 1;
                else {
                    int admin = login();                                    /*Login to verify*/
                    if (admin == 1) {
                        base.erase(base.begin() + option - 1);              /*Delete from vector and save that vector*/
                        saveCarVector(&base);
                    }
                }
            }
        }

        //Find user
        else if (option == 14) {
            system("cls");
            cout << "User ID:\t";
            int id;
            id = cin_value();
            findUser(&users, id);
            char blank;
            cout << "Type something to return: ";
            cin >> blank;
        }

        //Find car
        else if (option == 15) {
            system("cls");
            cout << "Car ID:\t";
            string id;
            getline(cin >> ws, id);
            findCarID(&base, id);
            char blank;
            cout << "Type something to return: ";
            cin >> blank;
        }

        //Orders list
        else if (option == 16) {

            bool back{ 0 };
            int idx{ 0 }; 
            while (!back) {
                pos = ios_base::beg; 
                eof = 0;
                oferts.clear();
                while (!eof)  oferts.push_back(loadOrder(&pos, &eof));      /*Orders list*/
                idx = 0;
                system("cls");
                for (auto i : oferts) { cout << ++idx << "\t";  i.print_list(); } 
                if (oferts[0].get_car_ID().length() < 2) {                  /*Chect if there is order*/
                    system("cls");
                    cout << "No orders" << endl;
                }
                cout << "\n\n0\tBACK";
                cout << "\n\nPrint details <order's number>: ";
                option = cin_value();
                if (!option) back = 1;
                else if (option > 0 && unsigned(option) <= oferts.size()) {
                    system("cls");
                    oferts[--option].print_list();
                    oferts[option].print_details(&users, &base);
                    cout << "\n\n\nType 1 to delete this order" << endl;
                    char buf;
                    cin >> buf;
                    if (buf == '1') {
                        string id = oferts[option].get_car_ID();
                        for (int i = 0; unsigned(i) <= base.size(); i++) {
                            if (base[i].get_ID() == id) { 
                                base[i].set_owner(0);                       /*Change owner*/
                                i = base.size() + 1;                        /*Force exit*/
                            } 
                        }
                        oferts.erase(oferts.begin() + option);
                        saveCarVector(&base);
                        saveOrderVector(&oferts);

                    }
                }
            }
        }

    }
    return 0;
}


