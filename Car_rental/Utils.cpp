#include "Utils.h"


int hello(int user) {
    system("cls");                                  /*Clear screen*/
    bool option_valid{ 0 };
    int option{ 0 };
    while (!option_valid) {

        cout << "**************************************" << endl;
        cout << "        _______\n       //  ||\\ \\\n _____//___||_\\ \\___\n )  _          _    \\\n |_/ \\________/ \\___|\n___\\_/________\\_/______\n";
        cout << "**************************************" << endl;
        cout << "Welcome in renting car system" << endl;
        cout << "How can i help you: \n 1. Base of our cars\n 2. New here? Register\n 3. Login" << endl;                                            /*Default options*/
        if (user) cout << " 4. Account details" << endl;                                                                                            /*Options for logged users*/
        if (user == 1) cout << " 11. Add car\n 12. Edit Car Information\n 13. Delate Car\n 14. Find User\n 15. Find Car\n 16. Order list" << endl;  /*Admin options*/
        cout << "\n\n 99. Exit\n";
        cout << "\n\n I want: ";

        option = cin_value();                       /*Only int value is acceptable*/

        if ((option > 0 && option <= 3) || option == 99) option_valid = 1;
        else if (option == 4 && user) option_valid = 1;
        else if (option >= 11 && option <= 16 && user == 1) option_valid = 1;
        else {                                      /*Incorrect option*/
            system("cls");
            cout << "Invalid option" << endl << endl;
        }

    }

    return option;
}


int cin_value() {
    int value{ 0 };
    while (!(cin >> value)) {                       /*If user types char, then cin>>value returns 0*/
        cout << "Write a number\n";
        cin.clear();                                /*Clear error flag*/
        cin.ignore(10000, '\n');                    /*Ignore written values*/
    }  
    return value;
}