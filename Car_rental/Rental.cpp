#include "Rental.h"



void Car::print_list() {
    cout << "MARK :\t" << m_mark;
    if (m_mark.length() < 8) cout << "\t";                              /*Test text lenght to keep clean tanelar look*/
    cout << "\tMODEL :\t" << m_model;
    if (m_model.length() < 8) cout << "\t";
    cout << "\tYEAR :\t" << m_year;
    cout << "\tPRICE :\t" << m_price << endl;
};

void Car::print_details(){
    cout << "\tMARK :\t\t" << m_mark << endl;
    cout << "\tMODEL :\t\t" << m_model << endl;
    cout << "\tYEAR :\t\t" << m_year << endl;
    cout << "\tMILEAGE :\t" << m_mileage << endl;
    cout << "\tPRICE :\t\t" << m_price << endl;
    cout << "\tBODY_TYPE :\t" << m_body_type << endl;
    cout << "\tDETAILS :\t" << m_details << endl;
};

void Car::print_admin() {
    cout << "\tID :\t\t" << m_ID << endl;
    cout << "\tOWNER :\t\t" << m_owner << endl;
    this->print_details();                                              /*There is no sense to write the same twice*/
}

void User::print_details() {
    cout << "\tUSERNAME :\t" << m_username << endl;
    cout << "\tPASSWORD :\t" << m_pass << endl;
    cout << "\tE-MAIL :\t" << m_email << endl;
    cout << "\tNAME :\t\t" << m_name << endl;
    cout << "\tSURNAME :\t" << m_surname << endl;
    cout << "\tADDRESS :\t" << m_address << endl;
    cout << "\tID NUMBER :\t" << m_IDnumber << endl;
    cout << "\tPHONE :\t\t" << m_phone << endl;
    cout << "\tBIRTHDATE :\t" << m_birthdate << endl;
};

void Order::print_list() {
    cout << "DATE :\t" << m_date;
    cout << "\tDAYS_NR :\t" << m_days;
    cout << "\tCAR :\t" << m_car_ID;
    cout << "\tUSER :\t" << m_user_ID<<endl;
}

void Order::print_details(vector<User>* users, vector<Car>* cars) {
    cout << "*****User information*******\n" << endl;
    findUser(users, m_user_ID);
    cout << "\n*****Car information*******" << endl;
    findCarID(cars, m_car_ID);
}

Car& Car::operator= (const Car& rhs) {
    if (this != &rhs) {
        m_mark = rhs.m_mark;
        m_model = rhs.m_model;
        m_ID = rhs.m_ID;
        m_year = rhs.m_year;
        m_mileage = rhs.m_mileage;
        m_price = rhs.m_price;
        m_body_type = rhs.m_body_type;
        m_details = rhs.m_details;
        m_owner = rhs.m_owner;
    }
    return *this;
}

User& User::operator= (const User& rhs) {
    if (this != &rhs) {
        m_ID = rhs.m_ID;
        m_username = rhs.m_username;
        m_pass = rhs.m_pass;
        m_email = rhs.m_email;
        m_name = rhs.m_email;
        m_surname = rhs.m_surname;
        m_address = rhs.m_address;
        m_IDnumber = rhs.m_IDnumber;
        m_phone = rhs.m_phone;
        m_birthdate = rhs.m_birthdate;
    }
    return *this;
}

Order& Order::operator= (const Order& rhs) {
    if (this != &rhs) {
        m_car_ID = rhs.m_car_ID;
        m_user_ID = rhs.m_user_ID;
        m_date = rhs.m_date;
        m_days = rhs.m_days;
    }
    return *this;
}

Car addCar() {

    system("cls");

    fstream plik;
    plik.open(source, ios::app);
    if (!plik.is_open()) {
        cout << "File don't exist \nPleas make sure that you configured exerything correctly"; 
        exit(1);                                                        /*Files are necessary to work properly so program can't be continued*/
    }

    string mark{ " " };
    cout << "MARK*:\t";
    getline(cin >> ws, mark);                                           /*getline because MARK could consist of two words*/
    plik << (mark + "\t");                                              /*Write to file separated by TAB*/

    string model{ " " };
    cout << "MODEL*:\t";
    getline(cin >> ws, model);
    plik << (model + "\t");

    string ID{ " " };
    cout << "ID*:\t";
    getline(cin >> ws, ID);
    plik << (ID + "\t");

    Car vehicle(mark, model, ID);                                       /*Minimal there should be three values*/

    cout << "Additional information - to skip write 0" << endl;

    int data{ 0 };

    cout << "YEAR: \t";
    data = cin_value();
    if (data) vehicle.set_year(data);                                   /*Test if user would skip information*/
    plik << (data);
    plik << ("\t");

    cout << "PRICE: \t";
    data = cin_value();
    if (data) vehicle.set_price(data);
    plik << (data);
    plik << ("\t");

    cout << "MILEAGE: \t";
    data = cin_value();
    if (data) vehicle.set_mileage(data);
    plik << (data);
    plik << ("\t");

    string information{ " " };
    cout << "BODY TYPE:\t";
    cin >> information;
    if (information != "0") vehicle.set_body_type(information);
    plik << (information + "\t");

    cout << "DETAILS:\t";
    getline(cin >> ws, information);
    if (information != "0") vehicle.set_details(information);
    plik << (information + "\t");

    cout << "OWNER [id]:\t";
    data = cin_value();
    vehicle.set_owner(data);
    plik << (data);
    plik << ("\t");
    plik << ("\n");                                                     /*Start new line for future data*/

    plik.close();
    return vehicle;

}

User addUser() {
    system("cls");

    fstream plik;
    plik.open(globalID, ios::in);
    if (!plik.is_open()) {
        cout << "File don't exist \nPleas make sure that you configured exerything correctly"; exit(1);
    }

    string information{ " " };
    int id{ 0 };

    getline(plik, information);                                         /*Get global and unique ID number*/
    id = stoi(information);
    plik.close();

    
    streampos pos{ ios_base::beg };
    bool eof{ 0 };   
    vector<User> users;  
    while (!eof)  users.push_back(loadUser(&pos, &eof));                /*Get users vector*/
    bool username_in_use = 1;                                           /*Unique login flag*/

    plik.open(usersBase, ios::app);
    if (!plik.is_open()) {
        cout << "File don't exist \nPleas make sure that you configured exerything correctly";
        exit(1);                                                        /*Exit when there is no file*/
    }


    cout << "Now make your username and password\n\n";
    string username{ " " };

    while (username_in_use) {
        cout << "Username*:\t";
        cin >> username;
        username_in_use = 0;
        for (auto i : users)                                            /*Iterate base to find same login - uniqueness test*/
            if (username == i.get_username()) {
                cout << "Username already in use\n Try another name\n\n";
                username_in_use = 1;
            }
    }

    string pass{ " " };
    cout << "Password*:\t";
    getline(cin >> ws, pass);



    string email{ " " };
    cout << "EMAIL*:\t\t";
    getline(cin >> ws, email);

    string name{ " " };
    cout << "REAL NAME*:\t";
    getline(cin >> ws, name);

    string surname{ " " };
    cout << "SURNAME*:\t";
    getline(cin >> ws, surname);

    string address{ " " };
    cout << "ADDRESS*:\t";
    getline(cin >> ws, address);

    string IDnumber{ " " };
    cout << "ID NUMBER*:\t";
    getline(cin >> ws, IDnumber);

    string phone{ " " };
    cout << "PHONE NUMBER*:\t";
    getline(cin >> ws, phone);

    string birthdate{ " " };
    cout << "BIRTHDATE*:\t";
    getline(cin >> ws, birthdate);

    User person(id, username, pass,email, name, surname, address, IDnumber, phone, birthdate);

    //Save in file
    plik << (id);
    plik << "\t";
    plik << (username + "\t");
    plik << (pass + "\t");
    plik << (email + "\t");
    plik << (name + "\t");
    plik << (surname + "\t");
    plik << (address + "\t");
    plik << (IDnumber + "\t");
    plik << (phone + "\t");
    plik << (birthdate + "\t\n");

    plik.close();

    plik.open(globalID, ios::out);
    if (!plik.is_open()) {
        cout << "File don't exist \nPleas make sure that you configured exerything correctly"; 
        exit(1);
    }
    plik << ++id;                                                       /*Increment ID for next user*/
    plik.close();

    return person;

}

void saveCarVector(vector<Car> *base) {

    fstream plik;
    plik.open(source, ios::out | ios::trunc);                           /*Clear file*/
    if (!plik.is_open()) {
        cout << "File don't exist \nPleas make sure that you configured exerything correctly"; 
        exit(1);
    }
    if (plik.is_open()) {
        
        for (auto i : *base) {
            plik << i.get_mark() << "\t";
            plik << i.get_model() << "\t";
            plik << i.get_ID() << "\t";
            plik << i.get_year() << "\t";
            plik << i.get_price() << "\t";
            plik << i.get_mileage() << "\t";
            plik << i.get_body_type() << "\t";
            plik << i.get_details() << "\t";
            plik << i.get_owner() << "\t\n";
        }

        plik.close();
    }
}

void saveUserVector(vector<User>* base) {

    fstream plik;
    plik.open(usersBase, ios::out | ios::trunc);                        /*Clear file - we hold everything in vector*/
    if (!plik.is_open()) {
        cout << "File don't exist \nPleas make sure that you configured exerything correctly"; 
        exit(1);
    }
    if (plik.is_open()) {

        for (auto i : *base) {
            plik << i.get_ID() << "\t";
            plik << i.get_username() << "\t";
            plik << i.get_pass() << "\t";
            plik << i.get_email() << "\t";
            plik << i.get_name() << "\t";
            plik << i.get_surname() << "\t";
            plik << i.get_address() << "\t";
            plik << i.get_IDnumber() << "\t";
            plik << i.get_phone() << "\t";
            plik << i.get_birthdate() << "\t\n";
        }
        plik.close();
    }
}

void saveOrderVector(vector<Order>* base) {                             /*Same as above*/

    fstream plik;
    plik.open(orders, ios::out | ios::trunc);
    if (!plik.is_open()) {
        cout << "File don't exist \nPleas make sure that you configured exerything correctly"; exit(1);
    }
    if (plik.is_open()) {

        for (auto i : *base) {
            plik << i.get_car_ID() << "\t";
            plik << i.get_user_ID() << "\t";
            plik << i.get_date() << "\t";
            plik << i.get_days() << "\t\n";
        }
        plik.close();
    }
}

int login() {
    system("cls");
    streampos pos{ ios_base::beg };                                     /*Read from beginning*/
    bool eof{ 0 };
    vector<User> users;
    while (!eof)  users.push_back(loadUser(&pos, &eof));                /*Import users*/

    string username{ " " };
    string pass{ " " };
    bool ok{ 0 };
    while (!ok) {
        cout<<"***************LOGIN***********\n\n Type 0 to exit\n\n";
        cout << "Username:\t";
        cin >> username;
        if (username == "0") return 0;
        cout << "Password:\t";
        cin >> pass;

        for (auto i : users) {                                          /*Check login and password*/
            if (username == i.get_username() && pass == i.get_pass()) {
                return i.get_ID();
            }
        }
        cout << "Password and username don't match\n Try again\n\n";
    }

    return 0;
}

Car loadCar(streampos* pos, bool* eof) {

    fstream plik;
    plik.open(source, ios::in);                                         /*Read from file*/
    if (!plik.is_open()) {
        cout << "File don't exist \nPleas make sure that you configured exerything correctly"; 
        exit(1);
    }
    plik.seekg(*pos);                                                   /*Place cursor at 'pos' position*/
    string mark{ " " };

    try {                                                               /*Try to avoid error*/
        getline(plik, mark, '\t');
        if (mark == "") throw "There is no cars";                       /*Empty file*/
    }
    catch (const char* ex) {                                            /*Error procedure*/
        std::cerr << ex << endl;
        *eof = plik.eof();
        Car vehicle("", "", "");
        return vehicle;
    }

    string model{ " " };
    getline(plik, model, '\t');

    string ID{ " " };
    getline(plik, ID, '\t');

    Car vehicle(mark, model, ID);

    string information{ " " };
    int data{ 0 };

    getline(plik, information, '\t');
    data = stoi(information);
    if (information != "0") vehicle.set_year(data);

    getline(plik, information, '\t');
    data = stoi(information);
    if (information != "0") vehicle.set_price(data);

    getline(plik, information, '\t');
    data = stoi(information);
    if (information != "0") vehicle.set_mileage(data);

    getline(plik, information, '\t');
    if (information != "0") vehicle.set_body_type(information);

    getline(plik, information, '\t');
    if (information != "0") vehicle.set_details(information);

    getline(plik, information, '\t');
    data = stoi(information);
    vehicle.set_owner(data);


    getline(plik, information);                                         /*Clear rest of line*/
    *pos = plik.tellg();

    getline(plik, information);                                         /*One more step to detect end of file*/
    *eof = plik.eof();

    plik.close();

    return vehicle;

}

User loadUser(streampos* pos, bool* eof) {                              /*Very similar to function above*/

    fstream plik;
    plik.open(usersBase, ios::in);
    if (!plik.is_open()) {
        cout << "File don't exist \nPleas make sure that you configured exerything correctly";
        exit(1);
    }
    plik.seekg(*pos);

    string information{ " " };
    int id{ 0 };

    try {
        getline(plik, information, '\t');
        if (information == "") throw "Empty file";
        id = stoi(information);
               
    }
    catch (const char* ex) {
        std::cerr << ex << endl;
        *eof = plik.eof();
        User person(0, "", "");
        return person;
    }

    string username{ " " };
    getline(plik, username, '\t');

    string pass{ " " };
    getline(plik, pass, '\t');

    User person(id, username, pass);

    getline(plik, information, '\t');
    person.set_email(information);

    getline(plik, information, '\t');
    person.set_name(information);

    getline(plik, information, '\t');
    person.set_surname(information);

    getline(plik, information, '\t');
    person.set_address(information);

    getline(plik, information, '\t');
    person.set_IDnumber(information);

    getline(plik, information, '\t');
    person.set_phone(information);

    getline(plik, information, '\t');
    person.set_birthdate(information);



    getline(plik, information);
    *pos = plik.tellg();

    getline(plik, information);
    *eof = plik.eof();

    plik.close();

    return person;

}

Order loadOrder(streampos* pos, bool* eof) {                              /*Very similar to function above*/

    fstream plik;
    plik.open(orders, ios::in);
    if (!plik.is_open()) {
        cout << "File don't exist \nPleas make sure that you configured exerything correctly"; 
        exit(1);
    }
    else {
        plik.seekg(*pos);

        string car_id{ " " };
        int user_id{ 0 };
        string date{ " " };
        int days{ 0 };
        string information{ " " };
        try {
            getline(plik, car_id, '\t');
            if (car_id == "") throw "File is empty";

            getline(plik, information, '\t');
            user_id = stoi(information);

            getline(plik, date, '\t');

            getline(plik, information, '\t');
            days = stoi(information);

            getline(plik, information);
            *pos = plik.tellg();

            getline(plik, information);
            *eof = plik.eof();
        }
        catch (const char* ex) {
            std::cerr << ex << endl;
            *eof = plik.eof();
        }

        plik.close();

        Order actual(car_id, user_id, date, days);
        *eof = plik.eof();
        return actual;
    }
    Order actual("", 0, "", 0);
    return actual;
}

void rentCar(Car* vehicle) {
    struct tm ltm;
    char buffer[80];
    time_t now = time(0);
    localtime_s(&ltm, &now);                                                /*Get time*/
    strftime(buffer, 80, "%d-%m-%Y %H:%M:%S", &ltm);                        /*Save into char buffor*/
    string time_str(buffer);                                                /*Char tab to string*/

    fstream plik;
    plik.open(orders, ios::app);
    if (!plik.is_open()) {
        cout << "File don't exist \nPleas make sure that you configured exerything correctly"; exit(1);
    }

    int user = login();
    if (!user) return;
    
    int nrDays;
    cout << "Select number of days :\t";
    nrDays = cin_value();
    int payment = nrDays * vehicle->get_price();
    cout << "\n\tORDER SUMMARY" << endl;
    cout << "YOUR CAR\n" << endl;
    vehicle->print_details();
    cout<<"\n\nOrder date: "<< time_str << endl;
    cout << "Number of days:\t" << nrDays << endl;
    cout << "Cost:\t" << payment << endl;

    char buff;
    cin >> buff;

    //Save into file
    plik << vehicle->get_ID() << "\t";
    plik << user << "\t";
    plik << time_str <<"\t";                                                /*Date of transaction*/
    plik << nrDays<<"\t\n";

    vehicle->set_owner(user);
    plik.close();
}

void deleteNotAva(vector<Car>* base) {
    int idx = 0;
    
    for (int i = 0; unsigned(i) <=  base->size(); i++) {
        if (i == base->size()) return;                                      /*When we achieve end of vector return*/
        if(((*base)[i].get_owner())){
            base->erase(base->begin() + i--);
        }
    }
}

void findUser(vector<User>* base, int id) {
    for (auto i : *base) if (i.get_ID() == id) { i.print_details(); return; }
    cout << "User not found" << endl;
}

void findCarID(vector<Car>* base, string id) {
    for (auto i : *base) if (i.get_ID() == id) { i.print_admin(); return; }
    cout << "Car not found" << endl;
}

vector<Car> sortit(vector<Car> base) {
    cout << "Sort By \n 1.MARK\n 2.MODEL\n 3.YEAR\n 4.PRICE\n\n-> ";
    int option;
    option = cin_value();
    switch (option) {                                                       /*Lambda functions*/
    case 1:         
        sort(base.begin(), base.end(), [](Car x, Car y) { return x.get_mark() < y.get_mark(); });   
        break;
    case 2:
        sort(base.begin(), base.end(), [](Car x, Car y) { return x.get_model() < y.get_model(); });
        break;
    case 3:
        sort(base.begin(), base.end(), [](Car x, Car y) { return x.get_year() < y.get_year(); });
        break;
    case 4:
        sort(base.begin(), base.end(), [](Car x, Car y) { return x.get_price() < y.get_price(); });
        break;
    }


    return base;
}