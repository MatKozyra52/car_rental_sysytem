# Opis projektu     
Realizując zadanie projektowe stworzyłem aplikacje konsolową, której zadaniem jest zarządzanie danymi potrzebnymi do funkcjonowania wypożyczalni samochodów. Program gromadzi informacje o posiadanych w firmie samochodach, użytkownikach, oraz dokonanych zamówieniach. Przetwarzanie wszystkich zapisanych informacji odbywa się z poziomu prostego menu tekstowego zaimplementowanego przy pomocy języka C++. Aplikacja została zaprojektowana w taki sposób, aby korzystać z niej mógł zarówno klient, jak i administracja firmy. Zależnie jednak od użytkownika program oferuje różne poziomy dostępu do danych. Niezalogowany użytkownik może zobaczyć listę dostępnych pojazdów, zarejestrować się bądź zalogować jeżeli posiada konto. Po zalogowaniu jest w stanie edytować swoje dane bądź wypożyczyć samochód z listy aktualnie dostępnych. Zalogowanie jako administrator udostępnia wiele dodatkowych funkcji pozwalających swobodnie modyfikować dane – w łatwy sposób z poziomu menu programu można dodawać/usuwać/edytować informacje o pojazdach, istnieje opcja wyszukiwania zarówno użytkowników jak i samochodów, a także tylko administracja ma wgląd do zamówień, które to właśnie są sercem każdej wypożyczalni.  Zaimplementowane zostały dodatkowo mechanizmy kontrolne chroniące program przed niepoprawnie wprowadzonymi danymi, bądź brakiem odpowiednich plików źródłowych wszystko po to by aplikacja działała stabilnie.    

# Project description   
To implement design task, I created console application whose main goal is to manage data in car rental system. The program collects information about owned cars, users and orders. Information processing is done from simple text menu implemented with the help of the C++. The application has been designed in such a way that both the client and the company's administration can use it. However, depending on the user, the program offers different data access level. A user who is not logged-in can see the list of available vehicles, register or log-in if he has an account already. After logging in, he is able to edit his data or rent a car from the list of currently available ones. Logging in as an administrator provides many additional functions that allow you to freely modify the data - you can easily add / delete / edit information about vehicles from the program menu, there is an option to search for both users and cars, and only the administration has access to orders. Additionally, control mechanisms have been implemented to protect program and let it work stably.    

# Instrukcja użytkownika    
## Przed pierwszym uruchomieniem    
Przed pierwszym uruchomieniem konieczne jest utworzenie dodatkowych plików .txt, które są niezbędne do prawidłowego uruchomienia aplikacji. Zarówno ścieżkę jak i nazwę plików można dowolnie modyfikować z poziomu kodu poprzez modyfikacje dyrektywy define w pliku ‘Rental.h’. Zakładając domyślne nazwy plików należy:  
 - Utworzyć plik cars.txt – może pozostać pusty
 - Utworzyć plik nextID.txt, oraz zmodyfikować jego zawartość wpisując do niego liczbę całkowitą większą od 1.
 - Utworzyć plik orders.txt – może również pozostać pusty
 - Utworzyć plik users.txt, oraz wpisać do niego informacje o administratorze – można wkleić:    
```
„1	admin   admin   x	x	x	x	x	x	x	   
“ 
```
   
Przy tak przygotowanym systemie plików można bezproblemowo uruchomić aplikację.   
## Obsługa programu       
Poruszanie się po programie sprowadza się do wybierania widocznych na ekranie funkcji poprzez wpisywanie odpowiadającego numeru.      
Opcje:     
**1.„Base of our cars”** – pozwala na wgląd do bazy dostępnych samochodów, czyli tych które nie są aktualnie przez nikogo wypożyczone. Po wybraniu tej opcji wyświetlana jest lista z głównymi parametrami pojazdów (marka, model, rok produkcji, cena wypożyczenia za dzień). Następnie możemy dowolnie posortować pojazdy, bądź uzyskać więcej informacji o danym egzemplarzu. Istnieje również możliwość wypożyczenia, ale wyłącznie dla użytkowników posiadających już konta. Podczas wypożyczenia wystarczy wyłącznie podać dane logowania, a także zdefiniować liczbę dni, na które chcemy wynająć pojazd.      
**2.„New here? Register”** – pozwala założyć konto. Wymagane informacje: nazwa użytkownika, hasło, e-mail, imię, nazwisko, adres, numer dowodu, numer telefonu oraz data urodzenia. Wszystkie te informacje są niezbędne.     
**3.„Login”** – służy do zalogowania się na konto poprzez podanie nazwy użytkownika oraz hasła, które to podaliśmy przy rejestracji.     
_Dodatkowa opcja dla zalogowanych użytkowników:_      
**4.„Account details”** – opcja pojawiająca się po zalogowaniu na konto.      
_Dodatkowe opcje dla administracji:_      
**11.„Add car”** – służy do wprowadzania nowych samochodów do bazy. Przy wprowadzaniu niezbędne jest podanie marki, modelu oraz id pojazdu rozumianego jako numer rejestracyjny. Dodatkowo można wprowadzić: rocznik, cenę za dzień, przebieg, typ nadwozia, numer aktualnego użytkownika, a także inne informacje, w sekcji details.     
**12.„Edit car Information”** – pozwala na zmianę wszystkich informacji o samochodzie, które wprowadziliśmy w procesie dodawania – może służyć do aktualizacji przebiegu, bądź zmiany jego statusu, gdyby był on niedostępny w danym momencie.     
**13.„Delete Car”** – program wyświetla listę pojazdów z której możemy wskazać pojazd, który będzie usunięty z bazy.     
**14.„Find User”** – opcja pozwala wyszukać użytkownika po przypisanym mu numerze identyfikacyjnym. Następnie wyświetlane są wszelkie dane na jego temat – przydatne gdy chcemy wyszukać dane kontaktowe do klienta.      
**15.„Find Car”**– na postawie numeru rejestracyjnego pojazdy wyświetlane są wszelkie informacje na jego temat.      
**16.„Order list”** – wyświetla listę aktywnych zamówień. Lista zawiera: datę wprowadzenia, liczbę dni, na które samochód został zarezerwowany, jak również dane samochodu i zamawiającego użytkownika. Po wybraniu odpowiedniego zamówienia wyświetlane pełne dane pojazdu, oraz klienta, jak również mamy możliwość usunięcia zamówienia, gdy np. zostanie ono już sfinalizowane.      
**99.„Exit”** – kończy działanie programu        


# Kompilacja
Standardowa kompilacja jest wystarczająca.



# Pliki źródłowe
Projekt składa się z następujących plików źródłowych:      
- Car_rental.cpp – w nim znajduje się funkcja main()   
- Rental.cpp, Rental.h  - deklaracja oraz implementacja klas Car, User, Order – z uwagi na silne zależności pomiędzy klasami postanowiłem nie rozdzielać ich pomiędzy różne pliki nagłówkowe  
- Utils.cpp, Utils.h – deklaracja oraz implementacja innych przydatnych funkcji  

# Zależności
Projekt nie zawiera dodatkowych zewnętrznych bibliotek.

# Opis klas   
Projekt zawiera następujące klasy:  
- Car – reprezentuje samochód i zawiera informacje o nim, a także metody
    - Konstruktor, setery, getery  - umożliwiają dostęp do prywatnych danych jak również utworzenie obiektu
    - void print_list() – wyświetla w jednej linii markę, model , przebieg oraz cenę samochodu
    - void print_details() – wyświetla wszystkie dostępne dane pojazdu z wyjątkiem jego ID oraz numeru aktualnego posiadacza
    - void print_admin() – wyświetla wszystkie informacje przechowywane w obiekcie
    - operator= - przeciążony operator przypisania

- User – przechowuje informacje o pojedynczym użytkowniku, oraz metody operujące na tych informacjach
    - Konstruktor, setery, getery  - umożliwiają dostęp do prywatnych danych jak również utworzenie obiektu
    - void print_details() – wyświetla wszystkie dane użytkownika poza automatycznie przypisywanym numerem ID
    - operator= - przeciążony operator przypisania

- Order – zawiera dane zamówienia oraz odpowiadające metody
    - Konstruktor, setery, getery  - umożliwiają dostęp do prywatnych danych jak również utworzenie obiektu
    - void print_list() – w jednej linii wyświetla dane o wypożyczeniu – data, liczba dni, id pojazdu, użytkownik 
    - void print_details(vector<User>* users, vector<Car>* cars) – wyświetla dodatkowe informacje o użytkowniku oraz pojeździe którego dotyczy zamówienie
    - operator= - przeciążony operator przypisania

# Zasoby
W projekcie wykorzystane są następujące pliki zasobów:  
- cars.txt – plik zawierający dane posiadanych pojazdów. Każdy pojazd zajmuje jedną linie pliku, a poszczególne informacje są rozdzielone tabulacjami. Zapisane dane:
    - marka,
    - model,
    - nr rejestracyjny,
    - rok produkcji,
    - cena za dzień wypożyczenia,
    - przebieg,
    - typ nadwozia,
    - dodatkowe informacje,
    - id klienta, który aktualnie posiada samochód (0 w przypadku gdy samochód jest dostępny.

- nextID.txt – przechowuje następny niezajęty numer ID dla nowego użytkownika

- orders.txt - plik zawierający dane zamówień. Każda oferta zajmuje jedną linie pliku, a poszczególne informacje są rozdzielone tabulacjami. Zapisane dane:
    - numer rejestracyjny samochodu,
    - ID klienta,
    - data transakcji,
    - liczba dni na który wypożyczamy.

- users.txt - plik zawierający dane zarejestrowanych użytkowników. Każda osoba zajmuje jedną linie pliku, a poszczególne informacje są rozdzielone tabulacjami. Zapisane dane:
    - nr ID,
    - login,
    - hasło,
    - adres e-mail,
    - imię,
    - nazwisko,
    - adres zamieszkania,
    - numer dowodu osobistego,
    - numer telefonu,
    - data urodzenia.

# Dalszy rozwój i ulepszenia
Główną wadą stworzonego programu jest brak odpowiedniego GUI. Co prawda aplikacja konsolowa jest w stanie w pełni obsłużyć funkcjonalność programu, jednak brak kolorowego i kształtnego interfejsu może skutecznie zniechęcić użytkownika swoją topornością.     
W następnej kolejności należałoby przenieś bazę danych poza plik lokalne tak aby różni klienci mogli łączyć się z wspólną bazą.
